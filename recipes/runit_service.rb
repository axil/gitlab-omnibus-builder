# Create a Runit service for gitlab-runner
#

username = node['gitlab-omnibus-builder']['username']

directory '/etc/sv/gitlab-runner' do
  recursive true
end

directory '/etc/sv/gitlab-runner/log' do
  recursive true
end

directory '/var/log/gitlab-runner' do
  owner username
  mode '0700'
end

template '/etc/sv/gitlab-runner/run' do
  mode '0755'
  variables({
    user: username,
    runner_dir: node['gitlab-omnibus-builder']['runner_dir'],
    home: node['gitlab-omnibus-builder']['home']
  })
end

template '/etc/sv/gitlab-runner/log/run' do
  mode '0755'
  source 'log-run.erb'
end

# This symlink tells Runit to enable the 'gitlab-runner' service
link '/etc/service/gitlab-runner' do
  to '/etc/sv/gitlab-runner'
end
