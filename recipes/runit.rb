# Set up Runit to supervise gitlab-runner
#

case node['platform_family']
when 'debian'
  package 'runit'
when 'rhel'
  # Install runit from source
  directory '/package' do
    mode '1755'
  end

  %w{glibc-static make gcc}.each do |pkg|
    package pkg
  end

  runit_tarball = '/package/runit-2.1.2.tar.gz'
  remote_file runit_tarball do
    source 'http://smarden.org/runit/runit-2.1.2.tar.gz'
    checksum '6fd0160cb0cf1207de4e66754b6d39750cff14bb0aa66ab49490992c0c47ba18'
    notifies :run, 'execute[extract runit tarball]', :immediately
  end

  execute 'extract runit tarball' do
    command "tar zpxf #{runit_tarball}"
    cwd '/package'
    action :nothing
    notifies :run, 'execute[install runit]', :immediately
  end

  execute 'install runit' do
    command 'package/install && package/install-man'
    cwd '/package/admin/runit-2.1.2'
    action :nothing
    notifies :run, 'execute[install runit executables]', :immediately
  end

  execute 'install runit executables' do
    command 'install -m750 /command/* /usr/sbin/'
    action :nothing
  end

  # The standard Runit directories would be /service and /sv. We put them under
  # /etc to match the Debian packages we use on the other platforms.
  directory '/etc/service'
  directory '/etc/sv'

  case node['platform_version']
  when /^6\./
    # Use Upstart to start Runit on RHEL 6
    cookbook_file '/etc/init/gitlab-runner-runsvdir.conf' do
      notifies :run, 'execute[start gitlab-runner-runsvdir]'
    end

    execute 'start gitlab-runner-runsvdir' do
      action :nothing
    end
  when /^7\./
    # Use SystemD  to start Runit on RHEL 7
    cookbook_file '/etc/systemd/system/default.target.wants/gitlab-runner-runsvdir.service' do
      notifies :run, 'execute[systemctl daemon-reload]', :immediately
      notifies :run, 'execute[systemctl start gitlab-runner-runsvdir]', :immediately
    end

    execute "systemctl daemon-reload" do
      action :nothing
    end

    execute "systemctl start gitlab-runner-runsvdir" do
      action :nothing
    end
  end
end
